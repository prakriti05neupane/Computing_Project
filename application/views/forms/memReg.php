
<!DOCTYPE html>
<html>
<head>
<title>Member Registration</title>
  
  <style type="text/css">
    @CHARSET "UTF-8";
    
.progress-bar {
    color: #333;
} 

* {
    -webkit-box-sizing: border-box;
     -moz-box-sizing: border-box;
          box-sizing: border-box;
  outline: none;
}

    .form-control {
    position: relative;
    font-size: 16px;
    height: auto;
    padding: 10px;
    @include box-sizing(border-box);

    &:focus {
      z-index: 2;
    }
  }

body {
  background: url("<?php echo base_url('assets/images/background.jpg'); ?>") no-repeat center center fixed;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
    background-size: cover;
}

.login-form {
  margin-top: 60px;
}

form[role=login] {
  color: #5d5d5d;
  background: #f2f2f2;
  padding: 26px;
  border-radius: 10px;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
}
  form[role=login] img {
    display: block;
    margin: 0 auto;
    margin-bottom: 35px;
  }
  form[role=login] input,
  form[role=login] button {
    font-size: 18px;
    margin: 16px 0;
  }
  form[role=login] > div {
    text-align: center;
  }
  
.form-links {
  text-align: center;
  margin-top: 1em;
  margin-bottom: 50px;
}
  .form-links a {
    color: #fff;
  }

  </style>
</head>

<body>
 <?php echo validation_errors(); ?>

<div class="container">
  
  <div class="row" id="pwd-container">
    <div class="col-md-4"></div>
    
    <div class="col-md-4">
      <section class="login-form">
        <form action= "<?php echo base_url(); ?>forms/memberSignUp" method="post" role="login" >

          <img src= "<?php echo base_url('assets/images/member.png'); ?>" class="img-responsive" alt="" />
          <input type="text" name="user" placeholder="Username" required class="form-control input-lg"/>

          <input type="password" class="form-control input-lg" id="password" name= "password" placeholder="Password" required="" /><br>
          
          <input type="text" name="fname" placeholder="Firstname" required class="form-control input-lg"/>

          <input type="text" name="lname" placeholder="Last name" required class="form-control input-lg"/><br>

          <input type="text" name="pst" placeholder="Post" required class="form-control input-lg"/>

          <input type="text" name="batch" placeholder="Batch" required class="form-control input-lg"/><br>

          <input type="text" name="em" placeholder="Email" required class="form-control input-lg"/>

          <input type="text" name="num" placeholder="Phone number" required class="form-control input-lg"/><br>

          <?php echo form_open_multipart(''); ?>
          <input type="file" name="userfile" required class="form-control input-lg"/>

          

          <div class="pwstrength_viewport_progress"></div>
                  
          <button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Sign Up</button>
          <div>
           <a href="#">Update email/Reset password</a>

          </div>
          
        </form>
        
</div>
</body>
</html>
