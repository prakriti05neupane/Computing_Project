
<!DOCTYPE html>
<html>
<head>
	<title>Admin Login</title>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/memberForm.css'); ?>">
</head>
<body>
<a href="#openAdLogin"><button>Admin Login</button></a>
<div id="openAdLogin" class="modalAdLogin">
    <div>
        <h2><b> Admin Login Details</b> <a href="#close" title="close" class="close"><button>X</button></a></h2>
        <hr>
        <form action= "<?php echo base_url(); ?>forms/adminData" method= "post">
            <label>Username:</label>
            <input type="email" name="email" required autofocus><br><br>
            <label>Password:</label>
            <input type="Password" name="password"><br><br>
            <button type="submit" name= "send">Login</button>
        </form>
        
    </div>
</div>
</body>
</html>