<!DOCTYPE html>
<html>
<head>
    <title>Member</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/memberForm.css'); ?>">
</head>
<body>
  <p>Club members and admin are only able to add the upcoming events so please login to your account.</p>
<a href="#openLogin"><button>Member Login</button></a>

<div id="openLogin" class="modalLogin">
    <div>
        <h2>Member login Details <a href="#close" title="close" class="close"><button>X</button></a></h2>
        <hr>
        <form method= "post" action= "<?php echo base_url(); ?>forms/memberSignIn">
            <label>Username</label>
            <input type="text" name="un" required><br><br>
            <label>Password</label>
            <input type="Password" name="pw"><br><br>
            <button type="submit" name= "send">Login</button>
        </form>
        
        

    </div>
</div>


</body>
</html>