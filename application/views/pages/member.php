<!DOCTYPE html>
<html>
<head>
	<title>Our members</title>

	<style type="text/css">
		
		.head{
			height: auto;
			width: 100%;
            text-align: left;
            background-color: white;
            display: inline-block;
           
		}

		.members{
			height: auto;
			width: 100%;
			text-align: button;
			background-color: green;
		}




		div #president{
			display: inline-block;
			padding: 2px;
			margin-top: 10px;
			margin-bottom: 10px;
			margin-left: 50px;
			margin-right: 200px;
			width: auto;
       }

		div #vice_president{
			display: inline-block;
			padding: 2px;
			margin-top: 10px;
			margin-bottom: 10px;
			margin-left: 50px;
			margin-right: 200px;
			width: auto;
		}


		div #mem{
			display: inline-block;
			margin-right: 70px;
		}

		div #addMem{
			display: inline-block;
			margin-right: 70px;
			cursor: pointer;
		}

		div #addMem:hover{
			background-color: brown;
		}


	</style>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/member.css'); ?>">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/darkly/bootstrap.min.css">
</head>
<h1 style="text-align: center;">Our Team Members</h1>
<body>
<div class= "head">
	
	<div id= "president">
	<img src= "<?php echo base_url('assets/images/img7.jpg'); ?>", height=150px; width=150px;/>
	<hr>
         <h3 style="color: black;">President: Zensa Shakya</h3>
	<a href="#loadWords" onclick="getcontent('president')"><button>Words from president</button></a>  
	</div>

	<div id= "vice_president">
	<img src= "<?php echo base_url('assets/images/img6.jpg'); ?>", height=150px; width=150px;/>
	<hr>
	<h3 style="color: black;">Vice-president: Ksitiz Devkota</h3>
	<a href= "#loadWords" onclick= "getcontent('vice_president')"><button>Words from vice-president</button></a>
	</div>
</div>
<div class= "members">
<div id= "mem">
	<h3>Co-ordinator</h3>
	<img src= "<?php echo base_url('assets/images/img10.png'); ?>", height=150px; width=150px;/><br>
	  Kushal Karki
	
</div>


	<div id= "mem">
	<h3>Volunteer leader</h3>
	<img src= "<?php echo base_url('assets/images/img8.jpg'); ?>", height=150px; width=150px;/><br>
	    Bikash Prajapati
	    </div>

	     <div id= "mem">
	<h3>Editor</h3>
	<img src= "<?php echo base_url('assets/images/img14.jpg'); ?>", height=150px; width=150px;/><br>
	    Erika Basnet
	    </div>

         <div id= "mem">
	<h3>Designer</h3>
	<img src= "<?php echo base_url('assets/images/img13.jpg'); ?>", height=150px; width=150px;/><br>
	    Sujan Panthi
	    </div>

	   <div id= "mem">
	<h3>Tresaurer</h3>
	<img src= "<?php echo base_url('assets/images/img9.jpg'); ?>", height=150px; width=150px;/><br>
	   Sofiya Banmala
	    </div>

	    <div id= "mem">
	<h3>Photographer</h3>
	<img src= "<?php echo base_url('assets/images/img17.jpg'); ?>", height=150px; width=150px;/><br>
	    Rajesh Heka
	    </div>

	  <div id= "mem">
	<h3>Editor</h3>
	<img src= "<?php echo base_url('assets/images/img12.jpg'); ?>", height=150px; width=150px;/><br>
	    Prakriti Neupane
	    </div>


	<div id= "mem">
	<h3>Photographer</h3>
	<img src= "<?php echo base_url('assets/images/img15.jpg'); ?>", height=150px; width=150px;/><br>
	    Rohit Awal
	    </div>

   
	    <div id= "mem">
	<h3>Member</h3>
	<img src= "<?php echo base_url('assets/images/img5.jpg'); ?>", height=150px; width=150px;/><br>
	   Ankita Khadka
	    </div>

	    <div id= "mem">
	<h3>Designer</h3>
	<img src= "<?php echo base_url('assets/images/img11.jpg'); ?>", height=150px; width=150px;/><br>
	    Aastha Gautam
	    </div>
       
    
     <div id= "mem">
	<h3>Designer</h3>
	<img src= "<?php echo base_url('assets/images/img16.jpg'); ?>", height=150px; width=150px;/><br>
	    Neeroj Kafle
	    </div>

	   <div id= "addMem">
	<h4>Add Members</h4>
	<img src= "<?php echo base_url('assets/images/add.png'); ?>", height=150px; width=150px;/><br>
	<a href="<?php echo base_url(); ?>forms/admin">Click here to add</a>
	    </div>
       
</div>

<div id= "loadWords">
<?php
$this->load->view('pages/words');
?>
</div>

</body>

</html>