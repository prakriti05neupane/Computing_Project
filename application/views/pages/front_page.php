<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />

	<title>Front Page</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/front.css'); ?>">
</head>
<head>
<div class= "header" id= "pageHeader">
<ul>
<li style= "float: left"><a class= "active" href= "#">HOME</a></li>
<li style= "float: left"><a href= "<?php echo base_url(); ?>forms/selectMembers">Admin Login</a></li>
<li style ="float: right"><a href= "#Loadfeedback">FEEDBACK</a></li>
<li style= "float: right"><a href= "#aboutContent">ABOUT US</a></li>
<li style= "float: right"><a href= "<?php echo base_url(); ?>eventsController/selectEvents">EVENTS</a></li>
<li style= "float: right"><a href= "<?php echo base_url(); ?>pages/loadMembers">MEMBERS</a></li>
<a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;

</a>

</ul> 
</div>
</head>	<br><br>
<body >
<div>
<img src= "../../assets/images/logo.jpg", height= 100px; width= 100px;/>

</div>

 <div id= "container" align="center">
      <img class="mySlides" src="../../assets/images/img1.jpg">
      <div class= "content"></div>
     <img class="mySlides" src="../../assets/images/img2.jpg">
      <div class= "content"></div>
      <img class="mySlides" src="../../assets/images/img3.jpg">
      <button class="w3-button w3-display-left" onclick="plusDivs(-1)">&#10094;</button>
<button class="w3-button w3-display-right" onclick="plusDivs(+1)">&#10095;</button>
    </div>
    <hr>

  <div style=" background-color: thistle" id= "aboutContent">
    <h1 style="text-align: center; font-family: Comic Sans MS">About Club</h1>
     <p style="font-size: 16px; font-family: Georgia">IT club of Softwarica is responsible for: <br><br><br> - Organizing IT research in Nepal regarding the latest technologies.<br><br>
      - Providing IT information to the Editorial Club.<br><br>
      - Arranging software development competition among the group of students at the college as well as among other colleges.<br><br>

      - Collecting information and versions related to IT technologies and conducting interview of the IT pioneers in Nepal.</p><br>

      <h2 style="text-align: left"><u>Membership Criteria</u></h2>
      
      <p style="font-size: 18px;">
      1. Students studying at first semester are not accepted to be member.<br><br>
      2. Must have some kind of knowledge and skills.<br><br>
      3. Students must have good result in their previous semester to join the club.<br><br>
      4. Must be highly dedicated to the club.<br><br>
       </p>
  </div>

  <div id= "Loadfeedback">
    <?php
    $this->load-> view('pages/feedback');
     ?>
  </div>
 
<script type="text/javascript">
    var slideIndex = 0;
carousel();

function carousel() {
    var i;
    var x = document.getElementsByClassName("mySlides");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none"; 
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1} 
    x[slideIndex-1].style.display = "block"; 
    setTimeout(carousel, 2000); // Change image every 2 seconds
}
  </script>

</body>
</html>
<?php

?>