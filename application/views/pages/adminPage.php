<!DOCTYPE html>
<html>
<head>
	<title>Admin dashboard</title>
	<style type="text/css">
		th{
			background-color:#4CAF50;
	        color: white;
	        height:45px;
		}

		tr{
			height: 45px;
		}

		table, th, td {
            border: 1px solid black;
        }
        table{
        	border-collapse: collapse;
        	width: 100%
        }

        button{
        	height: 30px;

        }
        tr:nth-child(even) {background-color: #f2f2f2}
        
       .message{
       	width: 100%;
       	height: 10px;

       }


	</style>
</head>
<body>
<h1>Welcome to admin panel</h1>
<?php
   if($this->session->flashdata('success_msg')){
   	?>
   	<div class= "message">
   	   <?php echo $this->session->flashdata('success_msg'); ?>
   	 </div>
   	 <?php
   }  
   ?> 
<table>
<tr>
<th><strong>Member ID</strong></th>
<th><strong> Username</strong></th>
<th><strong>Firstname</strong></th>
<th><strong>Lastname</strong></th>
<th><strong>Post</strong></th>
<th><strong>Batch</strong></th>
<th><strong>Email</strong></th>
<th><strong>Phone_no</strong></th>
<th colspan="2"><strong>Action</strong>
</tr>

<?php
if($getMembers->num_rows() > 0){
	foreach($getMembers->result() as $userdata){
		?>

	<tr>
	<td><?php echo $userdata->member_id; ?></td><td><?php echo $userdata->username; ?></td>
	<td><?php echo $userdata->first_name; ?></td>
	<td><?php echo $userdata->last_name; ?></td><td><?php echo $userdata->post; ?></td>
	<td><?php echo $userdata->batch; ?></td>
	<td><?php echo $userdata->email; ?></td>
	<td><?php echo $userdata->phone_no; ?></td>
    <td><button style="background-color: skyblue">Update</button>
    <td><a href="<?php echo site_url('membersController/delete_mem/'.$userdata->member_id); ?>" class= "delete_data" onclick= "return confirm('Are you sure you want to delete?');"><button style="color:white; background-color: red">Delete</button>
	</tr>
	<?php
   }

	}else{
		echo "result not found";

	}
	?>
	</table>

	
</body>
</html>