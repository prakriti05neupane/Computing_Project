<!DOCTYPE html>
<html>
<head>
	<title>Our Events</title>
</head>
<body>
<h1 style="align-content: center;"> Events organised by the club</h1>
<div= "container">
<h2>Share and Grow I</h2>
<img src="<?php echo base_url('assets/images/img1.jpg'); ?>", height= 300px; width= 300px;/>
</div>

<div class= "row">
<div style= "width:500px;margin:50px;">
<h4>Upcoming Events</h4>

<form>
<table border= '1' class= "table table-striped table-bordered">
<thead>
<tr><td><strong>Event ID</strong></td>
<td><strong>Event Name</strong></td>
<td><strong>Event Date</strong></td>
<td><strong>Event Time</strong></td>
<td><strong>Venue</strong></td>
</tr>
</thead>

<?php 
if($getEvents->num_rows() > 0){
  foreach($getEvents->result() as $row)
  {
    ?>
    <tbody>
   
               <tr> 
                 <td><?php echo $row->event_id; ?></td>
                 <td><?php echo $row->event_name; ?> </td>
                 <td><?php echo $row->event_date; ?></td>
                 <td><?php echo $row->event_time; ?></td>
                 <td><?php echo$row->venue; ?></td>
              </tr>
              <?php
      }
  }
else{
  echo "no record found";
}
?>

  </tbody> 
 </table>
 </form>

 </div>
 </div>
</body>
</html>