<?php
	class Pages extends CI_Controller{
		public function view($page= 'home'){
			if(!file_exists(APPPATH.'Views/pages/'.$page.'.php')){
				show_404();
			}
			$data['title']= ucfirst($page);
			$this->load->view('templates/header');
		    $this->load->view('pages/'.$page, $data);
		   
		}
		function loadMembers(){
			$this->load-> view("pages/member");
			$this->load-> view("pages/footer");
		}

		function  loadEvents(){
			$this->load-> view("forms/eventsForm");
			$this->load-> view("pages/events");
			$this->load-> view("pages/footer");

		}

		function about(){
			$this->load-> view("pages/about");
			$this->load-> view("pages/footer");
		}

		function feedback(){
			$this->load-> view("pages/feedback");
			$this->load-> view("pages/footer");
		}

 		public function addMember(){
  			$this->load->view('forms/memReg');
  			$this->load-> view('pages/footer'); 
		}

		function words(){
			$this->load->view("pages/words");
		}

		function adminPage(){
			$this->load->view("pages/adminPage");
			$this->load->view("pages/footer");
			
		}

	}