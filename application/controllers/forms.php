<?php
class forms extends CI_Controller{
    
public function admin(){
	$this->load-> view('forms/adminLogin');
	$this->load-> view('pages/footer'); 
}

public function adminData(){
  $this->load->model('adminDatabase');
  $query= $this->adminDatabase->adminValidate();

  if($query)//
  {
    $data= array(
        'email' => $this->input->post('email'), 'is_logged_in' => true
      );

        $this->session->set_userdata($data);
       redirect('site/members_area');
  }
  else{
    echo "Invalid username or password";
    $this->admin();
  }
}

public function member(){
	$this->load->view('forms/memberLogin');
  $this->load->view('forms/adminLogin');
	$this->load-> view('pages/footer'); 
}

public function memberSignUp(){
  $this->load->model('memberDatabase');
  $query= $this->memberDatabase->reg_Members();
   $this->load->library('form_validation');

   $this->form_validation->set_rules('user', 'Username', 'required');
   $this->form_validation->set_rules('password', 'Password', 'required');
   $this->form_validation->set_rules('fname', 'Firstname', 'required');
   $this->form_validation->set_rules('lname', 'Lastname', 'required');
   $this->form_validation->set_rules('em', 'Email', 'required');
   $this->form_validation->set_rules('num', 'Phone_no', 'required');

   if($this->form_validation->run()==False)
   {
    $this->load->view('forms/memReg');
    $this->load->view('pages/footer');
   }

   else{
    $this->load->view('forms/memberLogin');
    $this->load->view('pages/footer');
   }

}

public function memberSignIn(){
  $this->load->model('memberDatabase');
  $query= $this->memberDatabase->ValMemLogin();

  if($query)
  {
    $data= array(
      'username' => $this->input->post('un'), 'is_logged_in' => true
      );

      $this->session->set_userdata($data);
      redirect('site/loggedMembers');
  }
   else{
    echo "Invalid username or password";
    $this-> member();
   }
}

public function selectMembers(){
  $this->load->model('memberDatabase');

  $data['getMembers'] = $this->memberDatabase->getMembers();
   $this->load->view('pages/adminPage', $data);
   $this->load->view('pages/footer');

}


}


       