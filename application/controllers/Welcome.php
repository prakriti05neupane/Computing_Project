<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	public function frontPage(){
		$this->load->view('pages/front_page');
		//$this->load->view('pages/feedback');
		$this->load-> view('pages/footer');

	}

	public function members(){
		$this->load->view('pages/member');
	}
}
