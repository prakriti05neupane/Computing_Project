<?php
class Site extends CI_Controller{
	function members_area()
	{
		$this->load->view('forms/memReg');
		$this->load->view('pages/footer');
	}

	function loggedMembers()
	{
		$this->load->view('pages/member');
		$this->load->view('pages/footer');
	}
}