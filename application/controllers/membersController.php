<?php
class membersController extends CI_controller{
     
   public function selectMembers(){
  $this->load->model('memberDatabase');

  $data['getMembers'] = $this->memberDatabase->getMembers();
   $this->load->view('pages/adminPage', $data);
   $this->load->view('pages/footer');

}

	public function delete_mem($id){
		$this->load->model('memberDatabase');
	   $result= $this->memberDatabase->delete_members($id);
	   if($result){
	   	$this->session->set_flashdata('success_msg', 'Record deleted successfully');
	   }else{
	   	$this->session->set_flashdata('error_msg', 'Fail to delete record');
	   }
		redirect(base_url('membersController/deleted'));
	}

	public function deleted(){
		$this->selectMembers();
	}
}


?>