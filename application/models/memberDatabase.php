<?php
class memberDatabase extends CI_model{

	    public function reg_members(){
	    	$data= array(
	    		'username' => $this->input->post('user'),
	    		'password' => $this->input->post('password'),
	    		'first_name' => $this->input->post('fname'),
	    		'last_name' => $this->input->post('lname'),
	    		'post' => $this->input->post('pst'),
	    		'batch' => $this->input->post('batch'),
	    		'email' => $this->input->post('em'),
	    		'phone_no' => $this->input->post('num'),
	    		'image' => $this->input->post('userfile')

	    		);

	    	return $this->db->insert('members', $data);
	    }

	    public function ValMemLogin(){
	    	$this->db->where('username', $this->input->post('un'));
	   $this->db->where('password', $this->input->post('pw'));
	   $query= $this->db->get('members');

	   if($query->num_rows() == 1){
	   	  return true;
	    }
	}

	public function getMembers(){
		$query= $this->db->get('members');
		return $query;
	}

	function delete_members($id){
		$this->db->where("member_id", $id);
		$this->db->delete('members');
		if($this->db->affected_rows() > 0){
			return true;

		}else{
			return false;

		}

	}


	}
