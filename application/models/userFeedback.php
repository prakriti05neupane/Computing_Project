<?php

class userFeedback extends CI_model{
	function writeFeedback(){
		$userComment= array(
				'first_name' => $this->input->post('firstname'),
				'last_name' => $this->input->post('lastname'),
				'batch' => $this->input->post('batch'),
				'feedback' => $this->input->post('comment')

			);
		return $this->db->insert('feedback', $userComment);
	}
}

?>